import { Request, Response, NextFunction } from 'express';
import { User } from '../repository/User';

const userRep = new User(2);

export async function setRol(req: Request, res: Response): Promise<Response> {
  const { username, rol } = req.body;

  if (username && rol) {
    const result = await userRep.setRol(username, rol);

    if (result) {
      return res.json({
        message: 'Success update rol.'
      });
    }
  }

  return res.json({
    message: 'Role update failed. Check the parameters.'
  });
}

export async function updateUser(req: Request, res: Response): Promise<Response> {

  const { first_name,
    last_name, phone, email, username, address,
    gender, birth_date, country_id,
    city, category, document_id, user_state } = req.body;

  const inputUser = {
    first_name,
    last_name,
    phone,
    email,
    address,
    gender,
    birth_date,
    country_id,
    city,
    category,
    document_id,
    username,
    user_state
  };

  const userJSON = JSON.stringify(inputUser);

  const result = await userRep.update(userJSON);

  if (result) {
    return res.json({
      message: 'Success update user. Change data of the current user log in.'
    });
  }

  return res.json({
    message: 'User update failed. Check the parameters.'
  });
}

export async function deleteUser(req: Request, res: Response): Promise<Response> {

  const result = await userRep.delete();

  if (result) {
    return res.json({
      message: 'Success delete user.'
    });
  }

  return res.json({
    message: 'User delete failed. First run getUserByUsername().'
  });
}

export async function getUserByUsername(req: Request, res: Response): Promise<Response>{

  const { username } = req.body;

  const result = await userRep.getUserByUsername(username);

  if (result){
    return res.json({
      message: 'Success: User found.',
      user: result
    });
  }

  return res.json({
    message: 'User not found.'
  });

}

export async function changePassword(req: Request, res: Response): Promise<Response> {

  const { username, password } = req.body;

  const result = await userRep.changePassword(username, password);
  if (result) {
    return res.json({
      message: 'Success changes password.'
    });
  }

  return res.json({
    message: 'Change password failed. Check the parameters.'
  });

}

export function currentUser(req: Request, res: Response): Response {
  const user = req.app.locals.user;

  if (user) {
    userRep.setId(user.id);

    return res.json({
      message: 'Current user',
      user: userRep.userView(user)
    });
  }
  return res.json({
    message: 'Current user not exist.'
  });
}
