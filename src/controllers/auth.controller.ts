import { Request, Response, NextFunction } from 'express';
import passport from 'passport';
import { IVerifyOptions } from 'passport-local';
import { User } from '../repository/User';

const userRep = new User(2);

export function signinUser(req: Request, res: Response,
  next: NextFunction) {
  passport.authenticate('local.signin', (err: Error, user: any, info: IVerifyOptions) => {
    if (err) {
      return res.json({
        message: 'Error: ' + err.message
      });
    }
    if (!user) {
      return res.json({
        message: 'Error: ' + info.message
      });
    }
    req.logIn(user, (err) => {
      if (err) {
        return res.json({
          message: 'Error: ' + err.message
        });
      }
      return res.json({
        message: 'Success: ' + info.message,
        user: userRep.userView(user) 
      });
    })

  })(req, res, next);
}

export function signupUser(req: Request, res: Response,
  next: NextFunction) {
  passport.authenticate('local.signup', (err: Error, user: any, info: IVerifyOptions) => {
    if (err) {
      return res.json({
        message: 'Error: ' + err.message
      });
    }
    if (!user) {
      return res.json({
        message: 'Error: ' + info.message
      });
    }
    return res.json({
      message: 'Success: ' + info.message,
      user: userRep.userView(user)
    });
  })(req, res, next);
}

export function close(req: Request, res: Response): Response {
  req.logOut();
  return res.json({
    message: 'Session terminated.'
  });
}