import bcrypt from 'bcryptjs';

const encryptPassword = async (password: string) => {
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, salt);
  return hash;
};

const matchPassword = async (password: string, savedPassword: string) => {
  try {
    return await bcrypt.compare(password, savedPassword);
  } catch (e) {
    console.log(e);
  }
};

export default { encryptPassword, matchPassword };
