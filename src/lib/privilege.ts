import { Request, Response, NextFunction } from 'express';

export const hasPrivilege = (req: Request, res: Response, next: NextFunction) => {
  if (req.app.locals.user.rol === 1) {
    return next();
  }
  return res.status(401).send({ message: "The user is not authorized." });
};

export default { hasPrivilege };