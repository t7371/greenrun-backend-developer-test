import passport from 'passport';
import passportLocal from 'passport-local';
import pool from '../database';
import helpers from '../lib/helpers';

const LocalStrategy = passportLocal.Strategy;
const isDelete = 0;

passport.serializeUser<any, any>((req, user, done) => {
  done(undefined, user);
});

passport.deserializeUser((user:any, done) => {
  //console.log(id);
  //const rows = await pool.query('SELECT * FROM users WHERE id = ?', [id]);
  done(undefined, user);
})

passport.use('local.signin', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password'
},(username, password, done) => {
    pool.query('SELECT * FROM users ' +
      'WHERE username LIKE ? AND deleted = ?', [username, isDelete], async function (err: any, result: any) {
        if (err) { return done(err); }
        if (result.length > 0){
          const user = result[0];
          const validPassword = await helpers.matchPassword(password, user.password); 
          if (validPassword) {
            return done(undefined, user, { message: 'Welcome ' + user.username });
          } else {
            return done(undefined, false, { message: 'Incorrect password.' });
          }
        }
        else {
          return done(undefined, false, { message: 'This username does not exists.' });
        }
      });
  }));

passport.use('local.signup', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true
},(req: any, username, password, done) => {
    const { first_name,
      last_name, phone, email, address,
      gender, birth_date, country_id,
      city, category, document_id, user_state } = req.body;

    pool.query('SELECT * FROM users WHERE ' +
      'username LIKE ?', [username], async function (err: any, result: any) {
        if (err) { return done(err); }
        if (result.length > 0){
          return done(undefined, false, {message: "There is already a account that belongs to you."});    
        }
        else {
          const id = 0;
          const newUser = {
            id,
            first_name,
            last_name,
            phone,
            email,
            address,
            gender,
            birth_date,
            country_id,
            city,
            category,
            document_id,
            username,
            password,
            user_state
          };
          newUser.password = await helpers.encryptPassword(password);
          newUser.category = parseInt(newUser.category);

          pool.query('INSERT INTO users SET ?',
          [newUser], function (err: any, result: any) {
            if (err) { return done(err); }
            newUser.id = result.insertId;
            return done(null, newUser, {message: 'Welcome ' + newUser.username });
          });
        }
      });
  }));

