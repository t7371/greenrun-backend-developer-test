import express from 'express';
import path from 'path';
import cors from 'cors';
import morgan from 'morgan';
import session from 'express-session'
import flash from "connect-flash";
import passport from 'passport';
import database from './keys';
import indexRoutes from './routes/index';
import authRoutes from './routes/authentication';
import userRoutes from './routes/user.route';


import * as passportConfig from './lib/passport';


const MySQLStore = require('express-mysql-session');

// Initializations
const app = express();

//Settings
app.set('port', process.env.PORT || 3000);

//Middlewares
app.use(session({
  secret: 'nodesession',
  resave: false,
  saveUninitialized: false,
  store: new MySQLStore(database)
}));
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
passportConfig;

// Global Variables
app.use((req, res, next) => {
  app.locals.user = req.user;
  next();
});

//Configuration headers and cors
app.use(cors());

// Routes
app.use('/api', indexRoutes);
app.use('/auth', authRoutes);
app.use('/user', userRoutes);

//Listen error

process.on('uncaughtException', (err) => {
  console.error('There was an uncaught error', err);
  process.exit(1); //mandatory (as per the Node docs)
});

export default app;



