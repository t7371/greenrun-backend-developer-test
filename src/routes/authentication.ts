import { Router } from 'express';
import { isLoggedIn, isNotLoggedIn } from '../lib/auth';
import { signinUser, signupUser, close } from '../controllers/auth.controller';

const router = Router();

router.route('/signin')
  .post(isNotLoggedIn, signinUser)

router.route('/signup')
  .post(signupUser)

router.route('/logout')
  .get(isLoggedIn, close)


export default router;


