import { Router } from 'express';
import { getData, createBests } from '../controllers/bets.controller';

const router = Router();

/* router.get('/', (req: Request, res: Response) => {
  res.render('index');
}); */

router.route('/bests')
  .post(createBests)
  .get(getData)



export default router;
