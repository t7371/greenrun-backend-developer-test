import { Router } from 'express';
import { isLoggedIn, isNotLoggedIn } from '../lib/auth';
import { hasPrivilege } from '../lib/privilege';
import { setRol, getUserByUsername, updateUser, currentUser } from '../controllers/user.controller';

const router = Router();

router.route('/rol')
  .post(isLoggedIn, hasPrivilege, setRol)

router.route('/retrieve')
  .post(isLoggedIn, hasPrivilege, getUserByUsername)

router.route('/update')
  .post(isLoggedIn, hasPrivilege, updateUser)

router.route('/current')
  .get(isLoggedIn, currentUser)
  .post(isLoggedIn, updateUser)

export default router;