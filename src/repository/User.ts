import pool from '../database';
import helpers from '../lib/helpers';

enum Operation {
  Create = 1,
  Update = 2,
}

export class User {
  private rol: Number = 0;
  private id: Number = 0;
  private isdelete: Number = 0;

  constructor(userRol: Number) {
    this.rol = userRol;
  }

  setId(id: Number){
    this.id = id;
  }
  
  async setRol(username: string, rol: Number): Promise<boolean> {
    const updateRol = {
      rol
    }

    const result = await pool.query('UPDATE users SET ? WHERE username LIKE ?',
      [updateRol, username]);

    if (result.affectedRows === 1){
      return true;
    }  
    return false;
  }

  private toMap(userJSON: string): Map<any, any> {
    const userMap = new Map();
    const userParse = JSON.parse(userJSON);

    for (const key in userParse) {
      if (userParse.hasOwnProperty(key)) {
        userMap.set(key, userParse[key]);
      }
    }
    return userMap;
  }

  async update(userJSON: string): Promise<boolean> {
    const updateDate = new Date().toISOString().slice(0, 19).replace('T', ' ');
    const userMap = this.toMap(userJSON);

    userMap.set('updated_at', updateDate);
    const objUser = Object.fromEntries(userMap);

    const result = await pool.query('UPDATE users SET ? WHERE id = ?',
      [objUser, this.id]);

    if (result.affectedRows === 1){
      return true;
    } 
    else {
      return false;
    }
  }

  async delete(): Promise<boolean> {
    const deleted = 1;
    const deleted_at = new Date().toISOString().slice(0, 19).replace('T', ' ');
    const delUser = {
      deleted,
      deleted_at
    }

    const result = await pool.query('UPDATE users SET ? WHERE id = ?',
      [delUser, this.id]);

    if (result.affectedRows === 1){
      return true;
    } 
    else {
      return false;
    }
  }

  userView(user: any): any {
    const first_name = user.first_name;
    const last_name = user.last_name;
    const phone = user.phone;
    const email = user.email;
    const address = user.address;
    const gender = user.gender;
    const birth_date = user.birth_date;
    const country_id = user.country_id;
    const city = user.city;
    const category = user.category;
    const document_id = user.document_id;
    const username = user.username;
    const user_state = user.user_state;

    const view = {
      first_name,
      last_name,
      phone,
      email,
      address,
      gender,
      birth_date,
      country_id,
      city,
      category,
      document_id,
      username,
      user_state
    };

    return view;
  }

  private async getUser(username: string): Promise<any>{
    const result = await pool.query('SELECT * FROM users WHERE ' +
      'username LIKE ? AND deleted = ?', [username, this.isdelete]);

    if (result.length > 0){
      const user = result[0];
      return user;
    }
    else {
      return undefined;
    }

  }

  async getUserByUsername(username: string): Promise<any> {
    
    const user = await this.getUser(username);
    if (user){
      this.id = user.id;
      return this.userView(user);
    }  
    else {
      return undefined;
    }
  
  }

  async changePassword(username: string, pwd: string): Promise<boolean> {
    const password = await helpers.encryptPassword(pwd);
    const updatePassword = {
      password
    }
    const result = pool.query('UPDATE users SET ? WHERE username = ?',
    [updatePassword, username]);

    if (result.affectedRows === 1){
      return true;
    }
    else {
      return false;
    }
  }
}


export default { User };