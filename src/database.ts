import mysql, { Pool, QueryFunction } from 'mysql';
import { promisify } from 'util';
import { database } from './keys';

interface PromisifiedPool extends Omit<Pool, 'query'> {
  query: QueryFunction | Function;
}
const pool: PromisifiedPool = mysql.createPool(database);

pool.getConnection((err,
  connection) => {
  if (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.error('DATABASE CONNECTION WAS CLOSED');
    }
    if (err.code === 'ER_CON_COUNT_ERROR') {
      console.error('DATABASE HAS TO MANY CONNECTIONS');
    }
    if (err.code === 'ECONNREFUSED') {
      console.error('DATABASE CONNECTION WAS REFUSED');
    }
    if (err.code === 'ER_BAD_DB_ERROR') {
      console.error('UNKNOW DATABASE');
    }

  }

  if (connection) {
    connection.release();
    console.log('DB is connected');
  }
  return;
});

pool.query = promisify(pool.query);

pool.on('acquire', (connection) => {
  console.log('Connection %d acquired', connection.threadId);
});

pool.on('release', (connection) => {
  console.log('Connection %d released', connection.threadId);
});

export default pool;
