import app from './app';

async function main() {
  await app.listen(app.get('port'));
  console.log('Server is run in port', app.get('port'));
}

main();