CREATE DATABASE  IF NOT EXISTS `dbBets` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `dbBets`;
-- MySQL dump 10.13  Distrib 8.0.22, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: dbBets
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bets`
--

DROP TABLE IF EXISTS `bets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bets` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bet_option` int NOT NULL,
  `sport` varchar(20) NOT NULL,
  `status` smallint DEFAULT '0',
  `name` varchar(30) NOT NULL,
  `event_id` varchar(20) NOT NULL,
  `odd` decimal(3,1) DEFAULT '0.0',
  `result` smallint DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint DEFAULT '0',
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='Table for register the bets of user.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bets`
--

LOCK TABLES `bets` WRITE;
/*!40000 ALTER TABLE `bets` DISABLE KEYS */;
/*!40000 ALTER TABLE `bets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bets_user`
--

DROP TABLE IF EXISTS `bets_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bets_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `bet_id` int NOT NULL,
  `odd` decimal(3,1) DEFAULT '0.0',
  `amount` decimal(5,2) DEFAULT '0.00',
  `state` smallint DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint DEFAULT '0',
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_user_bets_user` (`user_id`),
  KEY `fk_bets_bets_user` (`bet_id`),
  CONSTRAINT `fk_bets_bets_user` FOREIGN KEY (`bet_id`) REFERENCES `bets` (`id`),
  CONSTRAINT `fk_user_bets_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='Table for register the bets of user.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bets_user`
--

LOCK TABLES `bets_user` WRITE;
/*!40000 ALTER TABLE `bets_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `bets_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pparams`
--

DROP TABLE IF EXISTS `pparams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pparams` (
  `idParamsType` smallint NOT NULL DEFAULT '0',
  `idParams` smallint NOT NULL DEFAULT '0',
  `parameter` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
  PRIMARY KEY (`idParamsType`,`idParams`) USING BTREE,
  CONSTRAINT `FK_pParams_pType` FOREIGN KEY (`idParamsType`) REFERENCES `pparamstype` (`idParamsType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='Table of params';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pparams`
--

LOCK TABLES `pparams` WRITE;
/*!40000 ALTER TABLE `pparams` DISABLE KEYS */;
INSERT INTO `pparams` VALUES (1,1,'admin'),(1,2,'user'),(2,1,'deposit'),(2,2,'withdraw'),(2,3,'bet'),(2,4,'winning'),(3,1,'active'),(3,2,'cancelled'),(3,3,'settled'),(4,1,'open'),(4,2,'won'),(4,3,'lost'),(5,1,'soccer'),(5,2,'basketBall');
/*!40000 ALTER TABLE `pparams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pparamstype`
--

DROP TABLE IF EXISTS `pparamstype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pparamstype` (
  `idParamsType` smallint NOT NULL,
  `key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`idParamsType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='Table for types parameters.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pparamstype`
--

LOCK TABLES `pparamstype` WRITE;
/*!40000 ALTER TABLE `pparamstype` DISABLE KEYS */;
INSERT INTO `pparamstype` VALUES (1,'rol'),(2,'category'),(3,'status'),(4,'state'),(5,'sport');
/*!40000 ALTER TABLE `pparamstype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transactions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `amount` decimal(5,2) DEFAULT '0.00',
  `category` smallint DEFAULT '0',
  `status` smallint DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint DEFAULT '0',
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_bet_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_user` (`user_id`),
  KEY `fk_bets_user_transactions` (`user_bet_id`),
  CONSTRAINT `fk_bets_user_transactions` FOREIGN KEY (`user_bet_id`) REFERENCES `bets_user` (`bet_id`),
  CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='Table for register of transactions.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `rol` smallint DEFAULT '0',
  `first_name` varchar(16) NOT NULL,
  `last_name` varchar(16) NOT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(60) NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `birth_date` date DEFAULT ((curdate() + interval 1 year)),
  `country_id` varchar(10) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `category` smallint DEFAULT '0',
  `document_id` varchar(20) NOT NULL,
  `user_state` smallint DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint DEFAULT '0',
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='Table for register users.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vcategory`
--

DROP TABLE IF EXISTS `vcategory`;
/*!50001 DROP VIEW IF EXISTS `vcategory`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vcategory` AS SELECT 
 1 AS `type`,
 1 AS `id`,
 1 AS `description`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vrol`
--

DROP TABLE IF EXISTS `vrol`;
/*!50001 DROP VIEW IF EXISTS `vrol`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vrol` AS SELECT 
 1 AS `type`,
 1 AS `id`,
 1 AS `description`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vsport`
--

DROP TABLE IF EXISTS `vsport`;
/*!50001 DROP VIEW IF EXISTS `vsport`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vsport` AS SELECT 
 1 AS `type`,
 1 AS `id`,
 1 AS `description`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vstate`
--

DROP TABLE IF EXISTS `vstate`;
/*!50001 DROP VIEW IF EXISTS `vstate`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vstate` AS SELECT 
 1 AS `type`,
 1 AS `id`,
 1 AS `description`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vstatus`
--

DROP TABLE IF EXISTS `vstatus`;
/*!50001 DROP VIEW IF EXISTS `vstatus`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vstatus` AS SELECT 
 1 AS `type`,
 1 AS `id`,
 1 AS `description`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'dbBets'
--

--
-- Final view structure for view `vcategory`
--

/*!50001 DROP VIEW IF EXISTS `vcategory`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vcategory` AS select `pparamstype`.`idParamsType` AS `type`,`pparams`.`idParams` AS `id`,`pparams`.`parameter` AS `description` from (`pparamstype` join `pparams` on((`pparamstype`.`idParamsType` = `pparams`.`idParamsType`))) where (`pparamstype`.`key` = 'category') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vrol`
--

/*!50001 DROP VIEW IF EXISTS `vrol`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vrol` AS select `pparamstype`.`idParamsType` AS `type`,`pparams`.`idParams` AS `id`,`pparams`.`parameter` AS `description` from (`pparamstype` join `pparams` on((`pparamstype`.`idParamsType` = `pparams`.`idParamsType`))) where (`pparamstype`.`key` = 'rol') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vsport`
--

/*!50001 DROP VIEW IF EXISTS `vsport`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vsport` AS select `pparamstype`.`idParamsType` AS `type`,`pparams`.`idParams` AS `id`,`pparams`.`parameter` AS `description` from (`pparamstype` join `pparams` on((`pparamstype`.`idParamsType` = `pparams`.`idParamsType`))) where (`pparamstype`.`key` = 'sport') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vstate`
--

/*!50001 DROP VIEW IF EXISTS `vstate`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vstate` AS select `pparamstype`.`idParamsType` AS `type`,`pparams`.`idParams` AS `id`,`pparams`.`parameter` AS `description` from (`pparamstype` join `pparams` on((`pparamstype`.`idParamsType` = `pparams`.`idParamsType`))) where (`pparamstype`.`key` = 'state') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vstatus`
--

/*!50001 DROP VIEW IF EXISTS `vstatus`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vstatus` AS select `pparamstype`.`idParamsType` AS `type`,`pparams`.`idParams` AS `id`,`pparams`.`parameter` AS `description` from (`pparamstype` join `pparams` on((`pparamstype`.`idParamsType` = `pparams`.`idParamsType`))) where (`pparamstype`.`key` = 'status') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-30 17:59:38
